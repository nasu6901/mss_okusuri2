
	<footer>
		<div class="ft-innr">
			<ul class="bnr-list">
				<li>
					<a href="http://www.mss-g.com/" target="_blank";>
						<img src="<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_mss_w250.gif" srcset="<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_mss_w250.gif 1x,<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_mss_w250@2x.gif 2x" alt="" />
					</a>
				</li>
				<li>
					<a href="http://www.ois92.co.jp/" target="_blank";>
						<img src="<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_okura_w250.gif" srcset="<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_okura_w250.gif 1x,<?php echo get_template_directory_uri(); ?>/images/bnr/bnr_okura_w250@2x.gif 2x" alt="" />
					</a>
				</li>
			</ul>
			<div class="ft-cmp clearfix">
				<div class="cmp-info">
					<p class="cmp-name">株式会社メディカルシステムサービス<br />お薬NET事業部</p>
					<p class="cmp-addr">〒160-0023<br />東京都新宿区西新宿7-9-18 第三雨宮ビル3階</p>
					<a href="http://www.mss-g.com/" target="_blank">運営会社</a>
					<a href="<?php echo esc_url( home_url()); ?>/policy">個人情報保護方針</a>
				</div>
				<div class="cta-tel">
					<div class="ic"><span class="icon-ic icon-ic-tel"></span></div>
					<p class="label"><strong>電話でのお問い合わせ</strong><br /><span class="mini">お気軽にお問い合わせください。</span></p>
					<div class="tel">
						<span class="tel-no">03-5937-1628</span>
						<p class="biz-hour">受付：平日 9:00〜17:30（土日祝日除く）</p>	
					</div>
				</div>
			</div>

			<p class="copy-right">© 2017 OKUSURI NET. All rights reserved. </p>
		</div>
	</footer>
	<!-- footer END -->	

</div>
<!-- // container END -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/apps.js"></script>

<?php wp_footer(); ?>

</body>
</html>
