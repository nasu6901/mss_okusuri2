<?php
/** The main template file. ページテンプレート */
/* Template Name: ページテンプレート */
get_header(); ?>

	<?php $pageslug = get_page($page_id)->post_name; ?>
	<div class="page-<?php echo $pageslug; ?>">
		<?php
			$pageslug = get_page($page_id)->post_name;
			get_template_part('pages/page', $pageslug); //ページテンプレート
		?>
	</div><!-- .page- -->

<?php get_footer(); ?>