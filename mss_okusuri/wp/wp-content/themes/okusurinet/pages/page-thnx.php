<?php
/** The main template file. 不動在庫移動 サンクスページ */
?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>
	
	<div class="bc_F6F8FA ">
		<section class="mem-intro">
			<h2>不動在庫移動のお申し込みを承りました。</h2>
			
			<p class="note">
				この度は、不動在庫移動のお申込みをいただき、誠にありがとうございます。お申込み内容を確認後、担当者より、あらためてご連絡させていただきますので、今しばらくお待ちください。
			</p>
			
			<div class="mem-nav">
				<ul>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/index_laravel.php/lara-list?user_id=')); ?><?php global $current_user; echo $current_user->user_login; // 加盟薬局名 表示 ?>" target="_blank"><span class="icon-ic icon-ic-file"></span><span>申込み履歴</span></a>
					</li>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/file/')); ?>guide_v01.pdf" target="_blank"><span class="icon-ic icon-ic-stack"></span><span>ご利用ガイド</span></a>
					</li>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/file/')); ?>faq_v01.pdf" target="_blank"><span class="icon-ic icon-ic-quest"></span><span>Q & A</span></a>
					</li>
<!--
					<li>
						<a class="bt-box purchase" href="<?php echo esc_url( home_url('/index_laravel_usr.php/lara-list?user_id=')); ?><?php global $current_user; echo $current_user->user_login; // 加盟薬局名 表示 ?>" target="_blank"><span class="icon-ic icon-ic-file"></span><span>購入可能リスト</span></a>
					</li>
-->
				</ul>
			</div>
		</section>
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
		 
	</div>
	<!-- // .bc_F6F8FA end -->

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->
