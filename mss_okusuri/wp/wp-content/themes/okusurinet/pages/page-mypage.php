<?php
/** The main template file. 加盟薬局ログインページ */

//確認ページで「戻る」を押した時に、入力した内容をそのまま表示させる記述です。	
session_start();

$rt = $_GET;

if (count($_SESSION) > 0) {
	$return = array();
	foreach ($_SESSION as $key => $value) {
		foreach ($value as $k => $v) {
			$return[$key][$k] = $v;
		}
	}
}

$m_1_01='';
$m_1_03='';
$m_1_04='';
$m_1_05='';

if( isset($_SESSION['m_1_01'])){ $m_1_01 = $_SESSION['m_1_01']; }
$_POST['m_1_02_return'] = $_SESSION['m_1_02_return'];
if( isset($_SESSION['m_1_03'])){ $m_1_03 = $_SESSION['m_1_03']; }
if( isset($_SESSION['m_1_04'])){ $m_1_04 = $_SESSION['m_1_04']; }
if( isset($_SESSION['m_1_05'])){ $m_1_05 = $_SESSION['m_1_05']; }
$_POST['m_1_06_return'] = $_SESSION['m_1_06_return'];
if( isset($_SESSION['m_1_07'])){ $m_1_07 = $_SESSION['m_1_07']; }
if( isset($_SESSION['m_1_08'])){ $m_1_08 = $_SESSION['m_1_08']; }
if( isset($_SESSION['m_1_09'])){ $m_1_09 = $_SESSION['m_1_09']; }
if( isset($_SESSION['m_1_10'])){ $m_1_10 = $_SESSION['m_1_10']; }
if( isset($_SESSION['m_1_11'])){ $m_1_11 = $_SESSION['m_1_11']; }
if( isset($_SESSION['m_1_12'])){ $m_1_12 = $_SESSION['m_1_12']; }
if( isset($_SESSION['m_1_13'])){ $m_1_13 = $_SESSION['m_1_13']; }

////// リスト //////
$dev_list = array(
''=>'--', '内服'=>'内服', '外用'=>'外用',
);
$str_list = array(
''=>'--', '先発品'=>'先発品', '後発品'=>'後発品',
);


?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>
	
	<div class="bc_F6F8FA ">
		<section class="mem-intro">
			<h2><?php global $current_user; echo $current_user->nickname; // 加盟薬局名 表示 ?></h2>
			<div class="mem-nav">
				<ul>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/index_laravel.php/lara-list?user_id=')); ?><?php global $current_user; echo $current_user->user_login; // 加盟薬局名 表示 ?>" target="_blank"><span class="icon-ic icon-ic-file"></span><span>申込み履歴</span></a>
					</li>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/file/')); ?>guide_v02.pdf" target="_blank"><span class="icon-ic icon-ic-stack"></span><span>ご利用ガイド</span></a>
					</li>
					<li>
						<a class="bt-box guide" href="<?php echo esc_url( home_url('/file/')); ?>faq_v01.pdf" target="_blank"><span class="icon-ic icon-ic-quest"></span><span>Q & A</span></a>
					</li>
<!--
					<li>
						<a class="bt-box purchase" href="<?php echo esc_url( home_url('/index_laravel_usr.php/lara-list?user_id=')); ?><?php global $current_user; echo $current_user->user_login; // 加盟薬局名 表示 ?>" target="_blank"><span class="icon-ic icon-ic-file"></span><span>購入可能リスト</span></a>
					</li>
-->
				</ul>
			</div>
		</section>
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
		 
	</div>
	<!-- // .bc_F6F8FA end -->

	<div class="bc_E1E5D6 ">
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>不動在庫移動申込フォーム</h2>
			<form class="apply-form" action="<?php echo esc_url( home_url('/')); ?>confirm" method="POST">
				
				<table class="unit-tbl">
					<caption>薬品１</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="jcode_1" id="jcode_1" value="<?php echo isset($rt['jcode_1']) ? $rt['jcode_1'] : ""; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="dev_1" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if(isset($rt['dev_1']) && $value == $rt['dev_1']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="pac_1" id="pac_1" placeholder="包装形態（例:PTP）" value="<?php echo isset($rt['pac_1']) ? $rt['pac_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="bquant_1" id="bquant_1" placeholder="売却数量 (総数)" value="<?php echo isset($rt['bquant_1']) ? $rt['bquant_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="company_1" id="company_1" value="<?php echo isset($rt['company_1']) ? $rt['company_1'] : ""; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="str_1" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if(isset($rt["str_1"]) && $value == $rt['str_1']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="pquant_1" id="pquant_1" placeholder="包装数量（例:15錠×6）" value="<?php echo isset($rt['pquant_1']) ? $rt['pquant_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="mprice_1" id="mprice_1" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo isset($rt['mprice_1']) ? $rt['mprice_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="name_1" id="name_1" value="<?php echo isset($rt['name_1']) ? $rt['name_1'] : ""; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="norm_1" id="norm_1" placeholder="規格（例:20mg）" value="<?php echo isset($rt['norm_1']) ? $rt['norm_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_1" id="state_1" placeholder="保存状態（開封済み）" value="<?php echo isset($rt['state_1']) ? $rt['state_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_1" id="experiod_1" placeholder="使用期限（2020/12）" value="<?php echo isset($rt['experiod_1']) ? $rt['experiod_1'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="note_1" id="note_1" placeholder="備考内容" value="<?php echo isset($rt['note_1']) ? $rt['note_1'] : ""; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 １ -->
				
				<table class="unit-tbl">
					<caption>薬品２</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="jcode_2" id="jcode_2" value="<?php echo isset($rt['jcode_2']) ? $rt['jcode_2'] : ""; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="dev_2" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if(isset($rt['dev_2']) && $value == $rt['dev_2']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="pac_2" id="pac_2" placeholder="包装形態（例:PTP）" value="<?php echo isset($rt['pac_2']) ? $rt['pac_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="bquant_2" id="bquant_2" placeholder="売却数量 (総数)" value="<?php echo isset($rt['bquant_2']) ? $rt['bquant_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="company_2" id="company_2" value="<?php echo isset($rt['company_2']) ? $rt['company_2'] : ""; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="str_2" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if(isset($rt["str_2"]) && $value == $rt['str_2']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="pquant_2" id="pquant_2" placeholder="包装数量（例:15錠×6）" value="<?php echo isset($rt['pquant_2']) ? $rt['pquant_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="mprice_2" id="mprice_2" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo isset($rt['mprice_2']) ? $rt['mprice_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="name_2" id="name_2" value="<?php echo isset($rt['name_2']) ? $rt['name_2'] : ""; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="norm_2" id="norm_2" placeholder="規格（例:20mg）" value="<?php echo isset($rt['norm_2']) ? $rt['norm_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_2" id="state_2" placeholder="保存状態（開封済み）" value="<?php echo isset($rt['state_2']) ? $rt['state_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_2" id="experiod_2" placeholder="使用期限（2020/12）" value="<?php echo isset($rt['experiod_2']) ? $rt['experiod_2'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="note_2" id="note_2" placeholder="備考内容" value="<?php echo isset($rt['note_2']) ? $rt['note_2'] : ""; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ２ -->

				<table class="unit-tbl">
					<caption>薬品３</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="jcode_3" id="jcode_3" value="<?php echo isset($rt['jcode_3']) ? $rt['jcode_3'] : ""; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="dev_3" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if(isset($rt['dev_3']) && $value == $rt['dev_3']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="pac_3" id="pac_3" placeholder="包装形態（例:PTP）" value="<?php echo isset($rt['pac_3']) ? $rt['pac_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="bquant_3" id="bquant_3" placeholder="売却数量 (総数)" value="<?php echo isset($rt['bquant_3']) ? $rt['bquant_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="company_3" id="company_3" value="<?php echo isset($rt['company_3']) ? $rt['company_3'] : ""; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="str_3" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if(isset($rt["str_3"]) && $value == $rt['str_3']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="pquant_3" id="pquant_3" placeholder="包装数量（例:15錠×6）" value="<?php echo isset($rt['pquant_3']) ? $rt['pquant_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="mprice_3" id="mprice_3" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo isset($rt['mprice_3']) ? $rt['mprice_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="name_3" id="name_3" value="<?php echo isset($rt['name_3']) ? $rt['name_3'] : ""; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="norm_3" id="norm_3" placeholder="規格（例:20mg）" value="<?php echo isset($rt['norm_3']) ? $rt['norm_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_3" id="state_3" placeholder="保存状態（開封済み）" value="<?php echo isset($rt['state_3']) ? $rt['state_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_3" id="experiod_3" placeholder="使用期限（2020/12）" value="<?php echo isset($rt['experiod_3']) ? $rt['experiod_3'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="note_3" id="note_3" placeholder="備考内容" value="<?php echo isset($rt['note_3']) ? $rt['note_3'] : ""; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ３ -->
				
				<table class="unit-tbl">
					<caption>薬品４</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="jcode_4" id="jcode_4" value="<?php echo isset($rt['jcode_4']) ? $rt['jcode_4'] : ""; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="dev_4" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if(isset($rt['dev_4']) && $value == $rt['dev_4']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="pac_4" id="pac_4" placeholder="包装形態（例:PTP）" value="<?php echo isset($rt['pac_4']) ? $rt['pac_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="bquant_4" id="bquant_4" placeholder="売却数量 (総数)" value="<?php echo isset($rt['bquant_4']) ? $rt['bquant_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="company_4" id="company_4" value="<?php echo isset($rt['company_4']) ? $rt['company_4'] : ""; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="str_4" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if(isset($rt["str_4"]) && $value == $rt['str_4']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="pquant_4" id="pquant_4" placeholder="包装数量（例:15錠×6）" value="<?php echo isset($rt['pquant_4']) ? $rt['pquant_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="mprice_4" id="mprice_4" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo isset($rt['mprice_4']) ? $rt['mprice_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="name_4" id="name_4" value="<?php echo isset($rt['name_4']) ? $rt['name_4'] : ""; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="norm_4" id="norm_4" placeholder="規格（例:20mg）" value="<?php echo isset($rt['norm_4']) ? $rt['norm_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_4" id="state_4" placeholder="保存状態（開封済み）" value="<?php echo isset($rt['state_4']) ? $rt['state_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_4" id="experiod_4" placeholder="使用期限（2020/12）" value="<?php echo isset($rt['experiod_4']) ? $rt['experiod_4'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="note_4" id="note_4" placeholder="備考内容" value="<?php echo isset($rt['note_4']) ? $rt['note_4'] : ""; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ４ -->
				
				<table class="unit-tbl">
					<caption>薬品５</caption>
					<tr>
						<td class="td-1">
							<input type="text" name="jcode_5" id="jcode_5" value="<?php echo isset($rt['jcode_5']) ? $rt['jcode_5'] : ""; ?>" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-2">
							<select name="dev_5" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $dev_list as $key => $value){
									 if(isset($rt['dev_5']) && $value == $rt['dev_5']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td class="td-3">
							<input type="text" name="pac_5" id="pac_5" placeholder="包装形態（例:PTP）" value="<?php echo isset($rt['pac_5']) ? $rt['pac_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td class="td-4">
							<input type="text" name="bquant_5" id="bquant_5" placeholder="売却数量 (総数)" value="<?php echo isset($rt['bquant_5']) ? $rt['bquant_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="company_5" id="company_5" value="<?php echo isset($rt['company_5']) ? $rt['company_5'] : ""; ?>" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<select name="str_5" class="ssBox validate[required]" aria-invalid="false">
								<?php
									foreach( $str_list as $key => $value){
									 if(isset($rt["str_5"]) && $value == $rt['str_5']){
									        echo "<option value='$value' selected>".$value."</option>";
									    }else{
									        echo "<option value='$value'>".$value."</option>";
									    }
									}
								?>
							</select>
						</td>
						<td>
							<input type="text" name="pquant_5" id="pquant_5" placeholder="包装数量（例:15錠×6）" value="<?php echo isset($rt['pquant_5']) ? $rt['pquant_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="mprice_5" id="mprice_5" placeholder="薬価 (小数点２位迄 記入)" value="<?php echo isset($rt['mprice_5']) ? $rt['mprice_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
							<span class="anno">※半角数字で入力</span>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="name_5" id="name_5" value="<?php echo isset($rt['name_5']) ? $rt['name_5'] : ""; ?>" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="norm_5" id="norm_5" placeholder="規格（例:20mg）" value="<?php echo isset($rt['norm_5']) ? $rt['norm_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="state_5" id="state_5" placeholder="保存状態（開封済み）" value="<?php echo isset($rt['state_5']) ? $rt['state_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
						<td>
							<input type="text" name="experiod_5" id="experiod_5" placeholder="使用期限（2020/12）" value="<?php echo isset($rt['experiod_5']) ? $rt['experiod_5'] : ""; ?>" aria-required="true" aria-invalid="false"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<input type="textarea" name="note_5" id="note_5" placeholder="備考内容" value="<?php echo isset($rt['note_5']) ? $rt['note_5'] : ""; ?>" />
						</td>
					</tr>
				</table><!-- 薬品 ５ -->
				
				<div class="submit-area clearfix">
					<div class="submit">
						<input id="submit" type="submit" value="確認画面へ進む" class="bt-submit" />
					</div>
				</div><!— // .submit-ara END —>
			</form>

		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->
	
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/customSelect/jquery.customSelect.min.js"></script>
<script>
jQuery(document).ready(function(){
	$('.ssBox').customSelect(); // セレクトボックス デザイン
});
</script>

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
	jQuery('.button_div label').addClass("check_css");
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->
