<?php
/** The main template file. 不動在庫移動申込 確認ページ */

//$_POST['name'] →$nameへ、$name= 値 といった具合で挿入されます
//値をセッションに入れる

session_start();
	
foreach ($_POST as $key => $value) {
  if (empty($value) || $value == "--") {
	  continue;
  }
  $sp = explode("_", $key);
  $_SESSION[$sp[1]][$sp[0]."_".$sp[1]] = $value;
 }


 $d_all_price = 0;
 $s_t_price = 0;
?>

<div id="contents">
	<?php if (is_user_logged_in()) : // ログイン中 ?>

	<div class="bc_E1E5D6 ">
	
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>申込内容確認</h2>
			<div class="conf-form">
				<table class="unit-tbl tbl-head">
					<tr>
						<td class="td-1">
							<span>JANコード</span>
						</td>
						<td class="td-2">
							<span>区分</span>
						</td>
						<td class="td-3">
							<span>包装形態</span>
						</td>
						<td class="td-4">
							<span>売却数量（総数）</span>
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<span>薬価小計</span>
							</div>
							<div class="s-price">
								<span>取引金額</span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span>製造会社</span>
						</td>
						<td>
							<span>先発</span>
						</td>
						<td>
							<span>包装数量</span>
						</td>
						<td>
							<span>薬価</span>
						</td>
					</tr>
					<tr>
						<td>
							<span>医薬品名</span>
						</td>
						<td>
							<span>規格</span>
						</td>
						<td>
								<span>保存状態</span>
						</td>
						<td>
							<span>使用期限</span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<p>備考内容</p>
						</td>
					</tr>
				</table><!-- テーブルヘッダー -->
				<form action="<?php echo esc_url( home_url('/')); ?>index_laravel.php/lara-form" method="post" id="oku_form">
			    <?php foreach ($_SESSION as $key => $val) { ?>
				<table class="unit-tbl">
					<tr>
						<td class="td-1">
							<span><?php echo $val["jcode_".$key]; ?></span>
							<input type="hidden" name="jcode_<?php echo $key; ?>" value="<?php echo $val['jcode_'.$key]; ?>" >
						</td>
						<td class="td-2">
							<span><?php echo $val["dev_".$key]; ?></span>
							<input type="hidden" name="dev_<?php echo $key; ?>" value="<?php echo $val['dev_'.$key]; ?>" >
						</td>
						<td class="td-3">
							<span><?php echo $val["pac_".$key]; ?></span>
							<input type="hidden" name="pac_<?php echo $key; ?>" value="<?php echo $val['pac_'.$key]; ?>" >
						</td>
						<td class="td-4">
							<span><?php echo $val["bquant_".$key]; ?></span>
							<input type="hidden" name="bquant_<?php echo $key; ?>" value="<?php echo $val['bquant_'.$key]; ?>" >
						</td>
						<td class="td-5" rowspan="3">
							<div class="d-price">
								<?php 
							    $kakeritu = 0.5;
							    if ($val["str_1"] == "先発品") {
								    $kakeritu = 0.6;
								}
								$d_price = floor($val["bquant_".$key]*$val["mprice_".$key]);
								$s_price = $d_price*$kakeritu;
								$d_all_price += $d_price;
								$s_t_price += $s_price;
								?>
								<span>¥ <?php echo number_format($d_price); ?></span>
								<input type="hidden" name="dprice_<?php echo $key; ?>" value="<?php echo $d_price; ?>" >
							</div>
							<div class="s-price">
								<span>¥ <?php echo number_format($s_price);  ?></span>
								<input type="hidden" name="sprice_<?php echo $key; ?>" value="<?php echo $s_price; ?>" >
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $val["company_".$key]; ?></span>
							<input type="hidden" name="company_<?php echo $key; ?>" value="<?php echo $val['company_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["str_".$key]; ?></span>
							<input type="hidden" name="str_<?php echo $key; ?>" value="<?php echo $val['str_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["pquant_".$key]; ?></span>
							<input type="hidden" name="pquant_<?php echo $key; ?>" value="<?php echo $val['pquant_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["mprice_".$key]; ?></span>
							<input type="hidden" name="mprice_<?php echo $key; ?>" value="<?php echo $val['mprice_'.$key]; ?>" >
						</td>
					</tr>
					<tr>
						<td>
							<span><?php echo $val["name_".$key]; ?></span>
							<input type="hidden" name="name_<?php echo $key; ?>" value="<?php echo $val['name_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["norm_".$key]; ?></span>
							<input type="hidden" name="norm_<?php echo $key; ?>" value="<?php echo $val['norm_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["state_".$key]; ?></span>
							<input type="hidden" name="state_<?php echo $key; ?>" value="<?php echo $val['state_'.$key]; ?>" >
						</td>
						<td>
							<span><?php echo $val["experiod_".$key]; ?></span>
							<input type="hidden" name="experiod_<?php echo $key; ?>" value="<?php echo $val['experiod_'.$key]; ?>" >
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<span><?php echo $val["note_".$key]; ?></span>
							<input type="hidden" name="note_<?php echo $key; ?>" value="<?php echo $val['note_'.$key]; ?>" >
						</td>
					</tr>
				</table><!-- 薬品 １ -->
				<?php } ?>
				
				
				<div class="total">
					<div class="total-d">
						<span class="ttl">薬価合計</span>
						<span class="price">¥ <?php echo number_format($d_all_price); ?></span>
					</div>
					<div class="total-s">
						<span class="ttl">取引金額合計</span>
						<span class="price">¥ <?php echo  number_format($s_t_price); ?></span>
					</div>
				</div>

				<div class="submit-area clearfix">
					<div class="submit">
						<button class="bt-back" id="back_btn">編集へ戻る</button>
						<input id="submit" type="submit" value="この内容で送信" class="bt-submit" />
					</div>
				</div><!— // .submit-ara END —>
				<?php $user_id = get_current_user_id(); ?>
				<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" >
				</form>
			</div><!-- //.conf-form  -->
		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->
<script>
jQuery(document).ready(function(){
    $("#back_btn").click(function(e) {
	$("#oku_form").attr("method", "GET");
        $("#oku_form").attr("action", "<?php echo esc_url( home_url('/')); ?>mypage#ask-form");
    });
});
</script>
	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
	<?php endif; ?>
</div><!-- // #contents END -->
