<?php
/** The main template file. 個人情報保護方針 ページ */
?>

<div id="contents">

	<div class="bc_E1E5D6 ">
		
		<section class="privacy">
			<h2>個人情報保護方針</h2>
			<div class="p-list">
				<section class="clause lv-3-1">
					<h3>個人情報の利用目的</h3>
					<p>お薬NETは、個人情報を次の目的のために利用します。これらの目的の他に利用する事はございません。</p>
					<ul>
						<li>1.お客様の維持・管理。</li>
						<li>2.関連会社・提携会社を含む各種セキュリティー管理。</li>
						<li>3.その他相互連絡に付随する業務。</li>
					</ul>
				</section>
				
				<section class="clause lv-3-2">
					<h3>収集する個人情報の種類</h3>
					<p>お薬NETは、お客様の住所、氏名、電話番号、携帯番号、メールアドレスなど、<br class="sp-non" />維持管理、諸サービスの提供に必要な個人情報を収集しております。</p>
				</section>
				
				<section class="clause lv-3-3">
					<h3>個人情報の提供</h3>
					<p>お薬NETは次の場合を除いて、ご本人の個人情報を外部に提供する事はありません。</p>
					<ul>
						<li>1.あらかじめ、ご本人が同意されている場合。</li>
						<li>2.利用目的の達成に必要な範囲において、業務を外部(当社代理店を含む)へ委託する場合。</li>
						<li>3.ご本人または公共の利益のために必要だと考えられる場合。</li>
						<li>4.ご指定住所へ情報の発送をする場合。</li>
						<li>5.その他法令に根拠がある場合。</li>
					</ul>
				</section>
				
				<section class="clause lv-3-4">
					<h3>個人情報の管理方法</h3>
					<p>お薬NETは、ご本人の個人情報を正確、最新なものにするよう常に適切な処置を講じております。<br class="sp-non" />
						また、法令等に要請される、組織的、技術的、人的な各安全管理処置を実施し、ご本人の個人情報への不正なアクセス、個人情報の紛失・破壊・改ざん・漏洩などを防止するため、万全を尽くしております。<br class="sp-non" />
						なお、当社の委託を受けて個人情報を取り扱う会社にも、同様に厳重な管理を行わせております。<br class="sp-non" />
						万一、個人情報に関する事故が発生した場合には、迅速かつ適切に対応致します。
					</p>
				</section>
				
				<section class="clause lv-3-5">
					<h3>個人情報の開示、訂正等、利用停止等</h3>
					<p>お薬NETは、ご本人の個人情報の開示、訂正等(訂正、追加、削除)、利用停止等(利用停止、消去)のご請求があった場合には、
						ご本人である事を確認させていただいた上で、当社業務に支障のない範囲で対応いたします。<br class="sp-non" />
						なお、ご要望にお応えできない場合は、ご本人に理由を説明致します。<br class="sp-non" />
						これらの具体的な請求手続きについては、下記のお問い合わせ先迄ご連絡ください。
					</p>
				</section>
				
				<section class="clause lv-3-6">
					<h3>＜お問い合わせ先＞</h3>
					<p>株式会社メディカルシステムサービス<br />お薬NET事業部　TEL:03-5937-1628<br class="sp-non" />
						メールでのお問合せは <a href="mailto:okusurinet@mss-group.co.jp">okusurinet@mss-group.co.jp</a> または、<a href="<?php echo esc_url( home_url()); ?>/#c-form" title="お問い合わせ">メールフォーム</a>より
					</p>
				</section>
				
			</div>
		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->

</div><!-- // #contents END -->