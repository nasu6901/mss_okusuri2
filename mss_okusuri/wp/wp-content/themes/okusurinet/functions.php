<?php
/* リセット系 /////////////////////////*/
remove_action('wp_head', 'wp_generator');

// バージョン更新を非表示にする
add_filter('pre_site_transient_update_core', '__return_zero');

// APIによるバージョンチェックの通信をさせない
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');

// フッターWordPressリンクを非表示に
function custom_admin_footer() {
 // echo '<a href="mailto:xxx@zzz">お問い合わせ</a>';
 }
add_filter('admin_footer_text', 'custom_admin_footer');

// セルフピンバックの禁止
function no_self_ping( &$links ) {
$home = get_option( 'home' );
	foreach ( $links as $l => $link )
	if ( 0 === strpos( $link, $home ))
	unset($links[$l]);
}
add_action( 'pre_ping', 'no_self_ping' );

// 管理バーの項目を非表示
function remove_admin_bar_menu( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'wp-logo' ); // WordPressシンボルマーク
	$wp_admin_bar->remove_menu('my-account'); // マイアカウント
	$wp_admin_bar->remove_menu( 'comments' ); // コメント
	$wp_admin_bar->remove_menu( 'new-media' ); // 新規 -> メディア
	$wp_admin_bar->remove_menu( 'new-link' ); // 新規 -> リンク
	$wp_admin_bar->remove_menu( 'new-page' ); // 新規 -> 固定ページ
	$wp_admin_bar->remove_node( 'new-mw-wp-form' ); // 新規 -> MW WP form
 }
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 201 );

if ( ! current_user_can( 'manage_options' ) ) {
	show_admin_bar( false );
}

// 管理バーのヘルプメニューを非表示にする
function my_admin_head(){
 echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
 }
add_action('admin_head', 'my_admin_head');

// 管理バーにログアウトを追加
function add_new_item_in_admin_bar() {
 global $wp_admin_bar;
 $wp_admin_bar->add_menu(array(
 'id' => 'new_item_in_admin_bar',
 'title' => __('ログアウト'),
 'href' => wp_logout_url()
 ));
 }
add_action('wp_before_admin_bar_render', 'add_new_item_in_admin_bar');

// 絵文字対応のスクリプトを無効化
function disable_emoji() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );  
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	//oEmbed関連のタグを削除
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
}
add_action( 'init', 'disable_emoji' );

// スマフォ分岐
function is_mobile() {
  $useragents = array(
    'iPhone',          // iPhone
    'iPod',            // iPod touch
    'Android',         // 1.5+ Android
    'dream',           // Pre 1.5 Android
    'CUPCAKE',         // 1.5+ Android
    'blackberry9500',  // Storm
    'blackberry9530',  // Storm
    'blackberry9520',  // Storm v2
    'blackberry9550',  // Storm v2
    'blackberry9800',  // Torch
    'webOS',           // Palm Pre Experimental
    'incognito',       // Other iPhone browser
    'webmate'          // Other iPhone browser
  );
  $pattern = '/'.implode('|', $useragents).'/i';
  return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

// iPad分岐
function is_ipad() {
    $is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
    if ($is_ipad) {
        return true;
    } else {
        return false;
    }
}

/* 記事のスラッグ名を自動的に記事IDにしたい */
function auto_post_slug( $slug, $post_ID, $post_status, $post_type ) {
    if ( preg_match( '/(%[0-9a-f]{2})+/', $slug ) ) {
        $slug = utf8_uri_encode( $post_type ) . '-' . $post_ID;
    }
    return $slug;
}
add_filter( 'wp_unique_post_slug', 'auto_post_slug', 10, 4  );

// 画像の挿入時に width と height を非表示
function remove_hwstring_from_image_tag( $html, $id, $alt, $title, $align, $size ) {
    list( $img_src, $width, $height ) = image_downsize($id, $size);
    $hwstring = image_hwstring( $width, $height );
    $html = str_replace( $hwstring, '', $html );
    return $html;
}
add_filter( 'get_image_tag', 'remove_hwstring_from_image_tag', 10, 6 );

// imgタグをpで囲わない
function remove_p_on_images($content){
    return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}
add_filter('the_content', 'remove_p_on_images');

// メディア挿入時 不要な属性を削除する
add_filter( 'image_send_to_editor', 'remove_image_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_image_attribute', 10 );

function remove_image_attribute( $html ){
	$html = preg_replace( '/(width|height)="\d*"\s/', '', $html );
	// $html = preg_replace( '/class=[\'"]([^\'"]+)[\'"]/i', '', $html );
	$html = preg_replace( '/title=[\'"]([^\'"]+)[\'"]/i', '', $html );
	$html = preg_replace( '/<a href=".+">/', '', $html );
	$html = preg_replace( '/<\/a>/', '', $html );
	return $html;
}

/* リセット系 END /////////////////////////*/

locate_template('lib/wpmem.php', true); // WP_members カスタム


function userrole_menus() { // 特権管理者 メニュー分岐
    $current_user = wp_get_current_user();
    if( $current_user->ID == "1" )  // 特権管理者の場合はメニューの削除をしない
	    {}
    else  // 特権管理者以外の場合はダッシュボードと投稿メニューを削除する
	    {
			remove_menu_page( 'index.php' );                  // ダッシュボード
			remove_menu_page( 'upload.php' );                 // メディア
			remove_menu_page( 'edit.php?post_type=page' );    // 固定ページ
			remove_menu_page( 'edit-comments.php' );          // コメント
			remove_menu_page( 'themes.php' );                 // 外観
			remove_menu_page( 'plugins.php' );                // プラグイン
			remove_menu_page( 'users.php' );                  // ユーザー
			remove_menu_page( 'tools.php' );                  // ツール
			remove_menu_page( 'options-general.php' );        // 設定
			remove_menu_page( 'edit.php?post_type=mw-wp-form' );       // MW WP form
	    }
}
add_action('admin_menu', 'userrole_menus');



// ログイン画面カスタマイズ 
// locate_template('lib/paging.php', true); // ページ送り

// バリデーションチェック
/*
function theme_name_scripts() {
 wp_enqueue_style( 'validationEngine.jquery.css', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css', array(), '1.0', 'all');
 wp_enqueue_script( 'jquery.validationEngine-ja.js', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-ja.min.js', array('jquery'), '2.0.0', true );
 wp_enqueue_script( 'jquery.validationEngine.js', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true );
}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
*/

// バリデーションチェック
// $Validation::set_ruleの引数は name属性値, バリデーション名, オプション
function my_validation_rule( $Validation, $data, $Data ) {
    $Validation->set_rule( 'your-name', 'noEmpty', array( 'message' => '※氏名を入力してください。' ) );-
    $Validation->set_rule( 'your-mail', 'noEmpty', array( 'message' => '※メールアドレスを入力してください。' ) );
	$Validation->set_rule( 'your-mail', 'mail', array( 'message' => '※メールアドレスの形式ではありません。' ) );
    return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-4', 'my_validation_rule', 10, 3 );


// ページタイトル生成
add_theme_support( 'title-tag' );
// タイトルの区切り文字を変更します。
function custom_document_title_separator( $sep ) {
	return '|';
}
add_filter( 'document_title_separator', 'custom_document_title_separator' );


// 再発行メール
function set_mail_from( $email ) {
    return 'mail@okusurinet.jp';
}
function set_mail_from_name( $email_from ) {
    return 'お薬NET';
}
add_filter( 'wp_mail_from', 'set_mail_from' );
add_filter( 'wp_mail_from_name', 'set_mail_from_name' );

?>