<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="imagetoolbar" content="no" /> <!-- IEのイメージツールバー 無効 -->	
<link rel="stylesheet" type="text/css" media="all" href="https://okusurinet.jp/wp/wp-content/themes/okusurinet/style.css" />
<script src="//code.jquery.com/jquery.js"></script>
	<script type="text/javascript" src="https://okusurinet.jp/wp/wp-content/themes/okusurinet/js/customSelect/jquery.customSelect.min.js"></script>
<script>
jQuery(document).ready(function(){
	$('.ssBox').customSelect(); // セレクトボックス デザイン
});
</script>
</head>

<body class="list-edit ">
<div id="wrap">

<section class="form-edit">
	<h1>不動在庫申し込み編集ページ</h1>
	
	<table class="unit-tbl tbl-head">
		<tr>
			<td class="td-5" rowspan="3">	
				<span>対応状況</span>
			</td>
			<td class="td-1">
				<span>JANコード</span>
			</td>
			<td class="td-2">
				<span>区分</span>
			</td>
			<td class="td-3">
				<span>包装形態</span>
			</td>
			<td class="td-4">
				<span>売却数量（総数）</span>
			</td>
		</tr>
		<tr>
			<td>
				<span>製造会社</span>
			</td>
			<td>
				<span>先発</span>
			</td>
			<td>
				<span>包装数量</span>
			</td>
			<td>
				<span>薬価</span>
			</td>
		</tr>
		<tr>
			<td>
				<span>医薬品名</span>
			</td>
			<td>
				<span>規格</span>
			</td>
			<td>
				<span>保存状態</span>
			</td>
			<td>
				<span>使用期限</span>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<p>備考内容</p>
			</td>
		</tr>
	</table><!-- テーブルヘッダー -->
	
	<div class="editor">
	<form class="apply-form" action="https://okusurinet.jp/index_laravel_admin.php/lara-admin-edit/{{ $zaiko->id }}" method="post">
		<table class="unit-tbl">
			<tr>
				<td class="td-5" rowspan="3">
					<select name="status" class="ssBox validate[required]" aria-invalid="false">
						@if($zaiko->status == 4)
						<option value="0">未対応</option>
						<option value="1">対応中</option>
						<option value="2">購入可</option>
						<option value="3">売却済</option>
						<option value="4" selected="selected">中止</option>
						@elseif($zaiko->status == 3)
						<option value="0">未対応</option>
						<option value="1">対応中</option>
						<option value="2">購入可</option>
						<option value="3" selected="selected">売却済</option>
						<option value="4">中止</option>
						@elseif($zaiko->status == 2)
						<option value="0">未対応</option>
						<option value="1">対応中</option>
						<option value="2" selected="selected">購入可</option>
						<option value="3">売却済</option>
						<option value="4">中止</option>
						@elseif($zaiko->status)
						<option value="0">未対応</option>
						<option value="1" selected="selected">対応中</option>
						<option value="2">購入可</option>
						<option value="3">売却済</option>
						<option value="4">中止</option>
						@else
						<option value="0" selected="selected">未対応</option>
						<option value="1">対応中</option>
						<option value="2">購入可</option>
						<option value="3">売却済</option>
						<option value="4">中止</option>
						@endif
					</select>
				</td>
				<td class="td-1">
					<input type="text" name="jcode" id="jcode_1" value="{{ $zaiko->jcode }}" placeholder="JANコード" aria-required="true" aria-invalid="false"/>
				</td>
				<td class="td-2">
					<select name="dev" class="ssBox validate[required]" aria-invalid="false">
						@if($zaiko->dev == "外用")
						<option value="外用" selected="selected">外用</option>
						<option value="内服">内服</option>
						@else
						<option value="外用">外用</option>
						<option value="内服" selected="selected">内服</option>
						@endif
					</select>
				</td>
				<td class="td-3">
					<input type="text" name="pac" id="pac_1" placeholder="包装形態（例:PTP）" value="{{ $zaiko->pac }}" aria-required="true" aria-invalid="false"/>
				</td>
				<td class="td-4">
					<input type="text" name="bquant" id="bquant_1" placeholder="売却数量 (総数)" value="{{ $zaiko->bquant }}" aria-required="true" aria-invalid="false"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="company" id="company_1" value="{{ $zaiko->company }}" placeholder="製造会社" aria-required="true" aria-invalid="false"/>
				</td>
				<td>
					<select name="str" class="ssBox validate[required]" aria-invalid="false">
						@if($zaiko->str == "先発品")
						<option value="先発品" selected="selected">先発品</option>
						<option value="後発品">後発品</option>
						@else
						<option value="先発品">先発品</option>
						<option value="後発品" selected="selected">後発品</option>
						@endif
					</select>
				</td>
				<td>
					<input type="text" name="pquant" id="pquant_1" placeholder="包装数量（例:15錠×6）" value="{{ $zaiko->pquant }}" aria-required="true" aria-invalid="false"/>
				</td>
				<td>
					<input type="text" name="mprice" id="mprice_1" placeholder="薬価 (小数点２位迄 記入)" value="{{ $zaiko->mprice }}" aria-required="true" aria-invalid="false"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="name" id="name_1" value="{{ $zaiko->name }}" placeholder="医薬品名" aria-required="true" aria-invalid="false"/>
				</td>
				<td>
					<input type="text" name="norm" id="norm_1" placeholder="規格（例:20mg）" value="{{ $zaiko->norm }}" aria-required="true" aria-invalid="false"/>
				</td>
				<td>
					<input type="text" name="state" id="state_1" placeholder="保存状態（開封済み）" value="{{ $zaiko->state }}" aria-required="true" aria-invalid="false"/>
				</td>
				<td>
					<input type="text" name="experiod" id="experiod_1" placeholder="使用期限（2020/12）" value="{{ $zaiko->experiod }}" aria-required="true" aria-invalid="false"/>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<input type="textarea" name="note" id="note_1" placeholder="備考内容" value="{{ $zaiko->note }}" />
				</td>
			</tr>
		</table><!-- 薬品 １ -->

		<div class="submit-area clearfix">
			<div class="submit">
				<input id="submit" type="submit" value="この内容で更新" class="bt-submit" />
			</div>
		</div><!— // .submit-ara END —>
		
	</form>
	
	</div><!-- //.editor -->
</section><!-- //.form-edit -->

</div><!-- //#wrap -->
</body>
</html>
