<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="imagetoolbar" content="no" /> <!-- IEのイメージツールバー 無効 -->	
<link rel="stylesheet" type="text/css" media="all" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<script src="//code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="https://okusurinet.jp/wp/wp-content/themes/okusurinet/js/jquery.floatThead.min.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="https://okusurinet.jp/wp/wp-content/themes/okusurinet/style.css" />

<!--
<script>
	$(function ()
	{
	var $table = $('.unit-tbl ');
	$table.floatThead();
	});
</script>
-->
</head>
	
<body class="admin-list">
<div id="wrap">
	
<h1>《不動在庫移動》管理側申込みリスト</h1>

<table class="unit-tbl" id="datatable-zaiko">
	<thead class="tr-head">
		<tr>
			<td><span>状況</span></td>
			<td><span>薬局名</span></td>
			<td><span>申請日</span></td>
			<td><span>依頼NO.</span></td>
			<td><span>医薬品名</span></td>
			<td><span>JANコード</span></td>
			<td><span>区分</span></td>
			<td><span>先後</span></td>
			<td><span>製造会社</span></td>
			<td><span>規格</span></td>
			<td><span>包装形態</span></td>
			<td><span>包装数量</span></td>
			<td><span>保存状態</span></td>
			<td><span>売却数量</span></td>
			<td><span>薬価</span></td>
			<td><span>薬価小計</span></td>
			<td><span>取引金額</span></td>
			<td><span>使用期限</span></td>
			<td><span>備考</span></td>
		    <td><span>編集</span></td>
		</tr>
	</thead>
</table>
</div><!-- //#wrap -->
<script>
$(function() {
    $('#datatable-zaiko').DataTable({
	    order: [[ 3, "desc" ]], // 3列目を昇順にする
		displayLength: 50, // 件数のデフォルトの値を50にする
		scrollY: 460,  // 縦スクロールバーを有効にする
	    oLanguage: {
		/* 日本語化設定 */
		sLengthMenu : "表示　_MENU_　件", // 表示行数欄(例 = 表示 10 件)
		oPaginate : { // ページネーション欄
		    sNext : "次へ",
		    sPrevious : "前へ"
		},
		sInfo : "_TOTAL_ 件中 _START_件から_END_件 を表示しています", // 現在の表示欄(例 = 100 件中 20件から30件 を表示しています)
		sZeroRecords : "表示するデータがありません", // 表示する行がない場合
		sInfoEmpty : "0 件中 0件 を表示しています", // 行が表示されていない場合
		sInfoFiltered : "全 _MAX_ 件から絞り込み" // 検索後に総件数を表示する場合
	    },
        processing: true,
        serverSide: true,
	searching: false,
        ajax: '{{ url("datatables/list") }}?user_id={{ $user_id }}&irai_id={{ $irai_id }}',
        columns: [
            { data: 'status', name: "status" },
            { data: 'display_name', name: 'display_name' },
            { data: 'created_at', orderable: "true" },
            { data: 'irai_id', orderable: "true" },
            { data: 'name', name: 'name' },
            { data: 'jcode', name: 'jcode' },
            { data: 'dev', name: 'dev' },
            { data: 'str', name: 'str' },
            { data: 'company', name: 'company' },
            { data: 'norm', name: 'norm' },
            { data: 'pac', name: 'pac' },
            { data: 'pquant', name: 'pquant' },
            { data: 'state', name: 'state' },
            { data: 'bquant', name: 'bquant' },
            { data: 'mprice', orderable: true },
            { data: 'dprice', orderable: true },
            { data: 'sprice', orderable: true },
            { data: 'experiod', orderable: true },
            { data: 'note', name: 'note' },
	    {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
});
</script>
</body>

</html>
