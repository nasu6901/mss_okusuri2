<div id="contents">
	
	<div class="bc_F6F8FA ">
		<section class="mem-intro">
			<h2><?php global $current_user; echo $current_user->nickname; // 加盟薬局名 表示 ?></h2>
		</section>
		
		<section class="mem-topics">
			<h2><span class="icon-ic-hexa"></span><span class="jpn">最新情報</span><span class="eng">INFORMATION</span></h2>
			<div class="u-l"></div>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				$args = array(
				     'post_type' => 'post', // 投稿タイプを指定
				     'posts_per_page' => 5, // 表示するページ数
				     'paged' => $paged, // 表示するページ数
			); ?>
			<?php $wp_query = new WP_Query( $args ); ?><!-- クエリの指定 -->
				<ul class="topics-list">
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<p class="p-date"><?php the_time( 'Y.m.d （D）' ); ?></p>
						<a class="p-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php wp_reset_postdata(); ?>
		</section>
		<!-- // .mem-topics end -->
		 
	</div>
	<!-- // .bc_F6F8FA end -->

	<div class="bc_E1E5D6 ">
	
		<a id="ask-form" name="ask-form"></a>
		<section class="ask-form">
			<h2>不動在庫移動申込フォーム</h2>
			<div class="apply-form">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php  // the_content(); ?>
				<?php endwhile; endif; ?>
				<?php echo do_shortcode('[mwform_formkey key="19"]'); ?>
			</div>

		</section>
	</div>
	<!-- // .bc_E1E5D6 end -->

	<?php else : // ログインしていないとき ?>
	
	<div class="bc_E1E5D6 ">
		
		<section class="mem-login">
			<h2>
				<div class="ic"><span class="icon-ic icon-ic-enter"></span></div>
				<span class="txt">加盟薬局ログイン</span>
			</h2>
			
			<?php // echo do_shortcode('[wp-members page="login"]'); ?>

			<?php echo do_shortcode('[wp-members page="members-area"]'); ?>
			
<script>
jQuery(document).ready(function(){
    jQuery('#log').attr('placeholder', 'ユーザー名');
    jQuery('#user').attr('placeholder', 'ユーザー名');
    jQuery('#pwd').attr('placeholder', 'パスワード(半角英数)');
    jQuery('#email').attr('placeholder', '登録済みのメールアドレス');
});
</script>
			
		</section>
		
	</div>
	<!-- // .bc_E1E5D6 end -->
	
</div><!-- // #contents END -->
