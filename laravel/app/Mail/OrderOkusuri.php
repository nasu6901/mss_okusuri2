<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\Zaiko;

class OrderOkusuri extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $zaiko)
    {
        //
	$this->order = $zaiko;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->order["templete"])
		    ->from($this->order["from"]["address"], $this->order["from"]["name"])
		    ->subject($this->order["subject"])
		    ->replyTo($this->order["replay"]["address"], $this->order["replay"]["name"])
                    ->with([
		        "display_name" => $this->order["user"]->display_name,
			"irai_id" => $this->order["zaiko"]->irai_id,            
                    ]);
    }
}
