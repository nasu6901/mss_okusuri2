<?php
	
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Zaiko extends Model {
	
	protected $table = "wp_zaiko";

	public function wp_users() {
		return $this->belongsTo("\App\Model\User", "user_id", "ID");
	}
	
	public static function getList($user_id, $irai_id = null) {
		$query = static::with("wp_users");
		return \DataTables::of($query)->filter(function($query) use ($user_id, $irai_id) {
			if ( !is_null($user_id)) {
				$user_id = User::where("user_login", $user_id)->first()->ID;
				$query->where('user_id', $user_id);
			}
			if ( ! is_null($irai_id)) {
				$query->where('irai_id', $irai_id);
			}
		})
		->addColumn("display_name", function($zaiko) {
			return $zaiko->wp_users->display_name;
	        })->addColumn('action', function ($zaiko) {
			return '<a href="https://okusurinet.jp/index_laravel_admin.php/lara-admin-edit/'.$zaiko->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
		})->editColumn('status', function($zaiko) {
                        $res = null;
			if ($zaiko->status == 4) {
			    $res = "中止";
			} elseif ($zaiko->status == 3) {
			    $res = "売却済";
			} elseif ($zaiko->status == 2) {
			    $res = "購入可";
                        } elseif ($zaiko->status) {
			    $res = "対応中";
                        } else {
			    $res = "未対応";
			}
                        return $res;
		})->make(true);
		
	}
	
}
