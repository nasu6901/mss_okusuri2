<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Zaiko;

class ListController extends Controller
{

    public function showList(Request $request) {
	$user_id = $request->input("user_id");
	if (is_null($user_id)) {
		$url = config("app.url");
		return redirect($url . "/mypage");
	}
	$data["user_id"] = $user_id;
        return view("list", $data); 
    }

    public function getList(Request $request) {
	$user_id = $request->input("user_id");
	return Zaiko::getList($user_id);
    }

}
