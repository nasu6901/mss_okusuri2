<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Zaiko;
use App\Model\User;

class AdminEditController extends Controller
{

	public function showEdit($id, Request $request) {
		$zaiko = Zaiko::find($id);
		return view("admin_edit", ["zaiko" => $zaiko]); 
	}

	public function doEdit($id, Request $request) {
		$post = $request->input();
		try {
			\DB::beginTransaction();
			
			$zaiko = Zaiko::find($id);
			foreach ($post as $zk => $z) {
				if (isset($zaiko->$zk)) {
					$zaiko->$zk = $z;
				}
			}
			$zaiko->save();
			\DB::commit();
		} catch (\Exception $e) {
			\DB::rollBack();
		}
		$url = config("app.url");
		return redirect($url . "/index_laravel_admin.php/lara-admin-list");
	}

}
