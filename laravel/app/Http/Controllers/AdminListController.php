<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Zaiko;

class AdminListController extends Controller
{

    public function showList(Request $request) {
	$user_id = $request->input("user_id");
	$data["irai_id"] = $request->input("irai_id");
	$user = \App\Model\User::where("ID", $user_id)->first();
	$data["user_id"] = $user_id;
	$data["user"] = $user;
        return view("admin_list", $data); 
    }

    public function getList(Request $request) {
	$irai_id = $request->input("irai_id", null);
	$user_id = $request->input("user_id", null);
	return Zaiko::getList($user_id, $irai_id);
    }

}
