<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderOkusuri;
use Illuminate\Support\Facades\Mail;

class OkusuriFormController extends Controller
{


    public function index(Request $request) {

		$post = $request->input();
		
		try {
			\DB::beginTransaction();
			$irai = new \App\Model\Irai();
			$irai->user_id = $post["user_id"];
			$irai->save();
			
			$tmp = [];
			foreach ($post as $key => $value) {
				$sp = explode("_", $key);
				if ($key != "user_id") {
					$tmp[$sp[1]][$sp[0]] = $value;
				}
			}
			
			foreach ($tmp as $k => $t) {
				$zaiko = new \App\Model\Zaiko();
				$zaiko->user_id = $post["user_id"];
				$zaiko->irai_id = $irai->id;
				foreach ($t as $zk => $z) {
					$zaiko->$zk = $z;
				}
				$zaiko->save();
			}
			\DB::commit();
			$user = \App\Model\User::find($post["user_id"]);
                        $mails = [
                            "owner" => [
                            	"to" => "okusurinet@mss-group.co.jp",
                                "from" => ["name" => "お薬NETホームページ", "address" => "mail@okusurinet.jp"],
				"subject" => "【不動在庫移動】申込み依頼がありました。",
			        "replay" => ["name" => "", "address" => $user->user_email],
				"templete" => "zaiko_mail",
                                "zaiko" => $zaiko,
				"user" => $user,
			    ],
			    "user" => [
                                "to" => $user->user_email,
                                //"to" => "speamint6901@gmail.com",
			        "from" => ["name" => "お薬NET", "address" => "okusurinet@mss-group.co.jp"],
       				"subject" => "不動在庫移動のお申込みを承りました",
		                "replay" => ["name" => "お薬NET", "address" => "okusurinet@mss-group.co.jp"],
				"templete" => "zaiko_mail_user",
                                "zaiko" => $zaiko,
				"user" => $user,
                            ]
                        ];
                        foreach($mails as $mail) {
			    Mail::to($mail["to"])
				->send(new OrderOkusuri($mail));
                        }
		} catch (\Exception $e) {
			\DB::rollBack();
		}
		$url = config("app.url");
		return redirect($url . "/thnx");
    }

}
