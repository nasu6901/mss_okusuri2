<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('lara-form', 'OkusuriFormController@index');
Route::get('lara-list', 'ListController@showList');
Route::get('lara-admin-list', 'AdminListController@showList');
Route::get('lara-admin-edit/{id}', 'AdminEditController@showEdit')
       ->where('id', '[0-9]+');
Route::post('lara-admin-edit/{id}', 'AdminEditController@doEdit')
       ->where('id', '[0-9]+');
Route::get('datatables/list', 'ListController@getList');
Route::get('datatables/list', 'AdminListController@getList');

Route::get('/', function () {
    return view('welcome');
});

